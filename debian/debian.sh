#!/usr/bin/env bash

cd || exit

apt update
apt upgrade
apt install -y \
  htop tig mc net-tools \
  ca-certificates curl \
  nginx \
  certbot python3-certbot-nginx \

sed -i -E 's/#alias ll=/alias ll=/' ~/.bashrc
sed -i -E 's/#alias la=/alias la=/' ~/.bashrc
sed -i -E 's/#alias l=/alias l=/' ~/.bashrc

adduser user54


install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y \
  docker-ce docker-ce-cli containerd.io docker-buildx-plugin \
  docker-compose-plugin docker-compose

groupadd docker || true
usermod -aG docker user54


# SSH Access
ssh-keygen
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP+ES+19hZxjH1/wG1wfxZUI8SHnfDl1vDEkxXs+WK9H5aisRqQi6Q9VIWEGBfma6K09UucCj3xC6FRKp1nUe2Mk2C2uyYK1qVOsTMXz9Ze46TpgOBomZxRpKqask8nuHUAbpT03Ym0Y11yir/CB2pfAFZBqHdVcdlmhhT8FnJxDOUJENJzEEJEdlslfkWGJ9+SUDPSgm5V+uI6CWXNWrVleqJ/ye3s6GmO31tgUf9qVFPIXIqCIgc/zCw8Pah3rK4PVMngRcv3b0uHgzz2F9BQFTmO7qUfZ+iPgn/mH5mjRrtHuGp0lsDNNfXb7ieAq6GswIxs66WWZYAA+4Kazu+eA081aEyfwSf5lEeh7dCqV/zdBQwDbKYkmR8G6RNUV0BhlO05pAPI3cOb6juhpUh5mAUfdpAjOdFYWyvkZqGJ33OQYtaSnVa70YvNf02LfzNrXd/C2/rvaKP7g3ZOZ2nR+hY10SISGYB3k9f/MH6/6otTqVpqm0H5HkFy7HeZJ9frVhCdm2MxaAx/Me+fEl1hEDSfyUus91kcnBAQodKskLkcwMzqfYrPOEdA0XAkqMLbRBj3RnAxj5FD9X9bZ6IgHdkXKXgqJd1g0Hd9yj5ymOPJmFnhGh5jajLppzqkIPJ/blZaiKYtjez4+SfjijC9By76lfv7T1n7cicsdhnyw== sapegin.in@gmail.com" > .ssh/authorized_keys
# Disable password authentication for SSH
sed -i -E 's/#?PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
service ssh restart


sed -i -E 's/user www-data;/user user54;/' /etc/nginx/nginx.conf

curl -O https://gitlab.com/Sapegin/templates/-/raw/master/debian/nginx/default.conf
curl -O https://gitlab.com/Sapegin/templates/-/raw/master/debian/nginx/_proxy_pass.conf
curl -O https://gitlab.com/Sapegin/templates/-/raw/master/debian/nginx/_ssl_params.conf

mv -f default.conf /etc/nginx/sites-available/default
mv -f _proxy_pass.conf /etc/nginx/_proxy_pass.conf
mv -f _ssl_params.conf /etc/nginx/_ssl_params.conf

service nginx reload


# certbot certonly -d XXX.it-private.ru --webroot -w /usr/share/nginx/html --agree-tos --email sapegin.in@gmail.com
