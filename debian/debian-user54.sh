#!/usr/bin/env bash

cd || exit

sed -i -E 's/#alias ll=/alias ll=/' ~/.bashrc
sed -i -E 's/#alias la=/alias la=/' ~/.bashrc
sed -i -E 's/#alias l=/alias l=/' ~/.bashrc

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
. ~/.nvm/nvm.sh

nvm install --lts

npm i -g npm pm2 yarn


ssh-keygen
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP+ES+19hZxjH1/wG1wfxZUI8SHnfDl1vDEkxXs+WK9H5aisRqQi6Q9VIWEGBfma6K09UucCj3xC6FRKp1nUe2Mk2C2uyYK1qVOsTMXz9Ze46TpgOBomZxRpKqask8nuHUAbpT03Ym0Y11yir/CB2pfAFZBqHdVcdlmhhT8FnJxDOUJENJzEEJEdlslfkWGJ9+SUDPSgm5V+uI6CWXNWrVleqJ/ye3s6GmO31tgUf9qVFPIXIqCIgc/zCw8Pah3rK4PVMngRcv3b0uHgzz2F9BQFTmO7qUfZ+iPgn/mH5mjRrtHuGp0lsDNNfXb7ieAq6GswIxs66WWZYAA+4Kazu+eA081aEyfwSf5lEeh7dCqV/zdBQwDbKYkmR8G6RNUV0BhlO05pAPI3cOb6juhpUh5mAUfdpAjOdFYWyvkZqGJ33OQYtaSnVa70YvNf02LfzNrXd/C2/rvaKP7g3ZOZ2nR+hY10SISGYB3k9f/MH6/6otTqVpqm0H5HkFy7HeZJ9frVhCdm2MxaAx/Me+fEl1hEDSfyUus91kcnBAQodKskLkcwMzqfYrPOEdA0XAkqMLbRBj3RnAxj5FD9X9bZ6IgHdkXKXgqJd1g0Hd9yj5ymOPJmFnhGh5jajLppzqkIPJ/blZaiKYtjez4+SfjijC9By76lfv7T1n7cicsdhnyw== sapegin.in@gmail.com" > .ssh/authorized_keys


curl -O https://gitlab.com/Sapegin/templates/-/raw/master/debian/user54/docker-compose.yml
docker-compose up -d

git config --global user.email "sapegin.in@gmail.com"
git config --global user.name "it-private"
