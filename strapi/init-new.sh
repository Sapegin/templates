#!/usr/bin/env bash
set -u
err() {
    echo "Error occurred:"
    awk 'NR>L-4 && NR<L+4 { printf "%-5d%3s%s\n",NR,(NR==L?">>>":""),$0 }' L=$(caller) $0
}
trap 'err' ERR

EMAIL=sapegin.in@gmail.com
DB_USER=dev-user
DB_PASS=dev-user
export APP_DIR=$(pwd)/${APP_NAME}

echo APP_NAME="${APP_NAME}"
echo APP_DIR="${APP_DIR}"

. ~/.nvm/nvm.sh

curl -O https://gitlab.com/Sapegin/templates/-/raw/master/strapi/scripts/db-create.sh
curl -O https://gitlab.com/Sapegin/templates/-/raw/master/strapi/scripts/get-port.sh
curl -O https://gitlab.com/Sapegin/templates/-/raw/master/strapi/scripts/template-nginx.sh
curl -O https://gitlab.com/Sapegin/templates/-/raw/master/strapi/scripts/template-pm2.sh

set -e

. get-port.sh
rm ./get-port.sh

. db-create.sh
rm ./db-create.sh

git clone -b "${TARGET_BRANCH}" "git@gitlab.com:${CI_PROJECT_PATH}.git" "${APP_NAME}"

# shellcheck disable=SC2164
cd "${APP_NAME}"

. ../template-nginx.sh
rm ../template-nginx.sh

. ../template-pm2.sh
rm ../template-pm2.sh

git add .
git commit -m "Deploy files ${TARGET_BRANCH}"
git push

## получить certificate letsencrypt & перезапустить nginx
echo "$NGINX_CONF" > nginx-configure.sh
echo "Run as root:"
echo "${NGINX_CONF}"
