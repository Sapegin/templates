#!/usr/bin/env bash

set +e

# сгенерировать порт 1401 и больше
BASE_PORT=1401
port=$BASE_PORT
isfree=$(netstat -taln | grep $port)
while [[ -n "$isfree" ]]; do
    port=$[port+1]
    isfree=$(netstat -taln | grep $port)
done

set -e

export APP_PORT=$port
echo APP_PORT="${APP_PORT}"
