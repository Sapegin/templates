#!/usr/bin/env bash

[ -d "${APP_NAME}" ] && rm -rf "${APP_NAME}"

yarn create strapi-app "${APP_NAME}" --no-run --ts\
  --dbclient=postgres \
  --dbhost=localhost \
  --dbport=5432 \
  --dbname=${APP_NAME} \
  --dbusername=${DB_USER} \
  --dbpassword=${DB_PASS} \
  --dbssl=false
