#!/usr/bin/env bash

yarn strapi telemetry:disable
yarn add @strapi/provider-email-nodemailer strapi-plugin-email-designer
