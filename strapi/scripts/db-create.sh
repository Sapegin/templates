#!/usr/bin/env bash

docker exec user54_postgres_1 psql -U postgres -c "CREATE DATABASE \"${APP_NAME}\";" || true
docker exec user54_postgres_1 psql -U postgres -c "ALTER DATABASE \"${APP_NAME}\" OWNER TO \"${DB_USER}\";" || true
