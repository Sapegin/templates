#!/usr/bin/env bash
# создать pm2.json -> запустить
export PM2_FILE="${TARGET_BRANCH}".pm2.json
curl -o- "https://gitlab.com/Sapegin/templates/-/raw/master/strapi/pm2.${TARGET_BRANCH}.template.json" > pm2.template.json

envsubst < pm2.template.json | xargs -0 echo > "${PM2_FILE}"

rm pm2.template.json
