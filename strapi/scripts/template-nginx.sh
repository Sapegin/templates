#!/usr/bin/env bash
# создать nginx site.conf
NGINX_FILE="${TARGET_BRANCH}".site.conf
curl -o- https://gitlab.com/Sapegin/templates/-/raw/master/strapi/site.template.conf > site.template.conf
envsubst < site.template.conf | xargs -0 echo > "${NGINX_FILE}"
rm site.template.conf

export NGINX_CONF="certbot certonly -d ${APP_NAME}.it-private.ru --webroot -w /usr/share/nginx/html --agree-tos --email $EMAIL && ln -sf $(pwd)/${NGINX_FILE} /etc/nginx/sites-enabled/link.${APP_NAME} && /usr/sbin/nginx -t && /usr/sbin/nginx -s reload"
